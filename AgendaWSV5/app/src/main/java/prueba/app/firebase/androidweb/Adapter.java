package prueba.app.firebase.androidweb;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import prueba.app.firebase.androidweb.Objetos.Contactos;

public class Adapter extends BaseAdapter
{
    private Context mContext;
    private List contactosList;
    private ArrayList<Contactos> contactosTemp;
    private ArrayList<Contactos> contactos;
    private LayoutInflater inflater;
    private int totalC = 0;
    private RelativeLayout relativeLayout;

    public Adapter(Context mContext, List<Contactos> contactosList)
    {
        this.mContext = mContext;
        this.contactosList = contactosList;
        this.contactos = new ArrayList<>();
        this.contactos.addAll(contactosList);
        totalC = this.contactos.size();
        this.contactosTemp = new ArrayList<>();
        this.contactosTemp.addAll(contactosList);
        this.inflater = LayoutInflater.from(this.mContext);
    }

    public class ViewHolder {
        TextView nombre, tel;
        Button btnBorra, btnModi;
        RelativeLayout  relativeLayout;
    }

    @Override
    public int getCount() { return this.contactosList.size(); }

    @Override
    public Object getItem(int position) { return this.contactosTemp.get(position); }

    @Override
    public long getItemId(int position) { return this.contactosTemp.get(position).get_ID(); }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        try
        {
            ViewHolder holder;
            if (view == null) {
                holder = new ViewHolder();
                view = inflater.inflate(R.layout.layout_contacto, null);
                holder.nombre = view.findViewById(R.id.lblNombreContacto);
                holder.tel = view.findViewById(R.id.lblTelefonoContacto);
                holder.btnBorra = view.findViewById(R.id.btnBorrar);
                holder.btnModi = view.findViewById(R.id.btnModificar);
                holder.relativeLayout = view.findViewById(R.id.rlC);
                view.setTag(holder);
            }
            else {
                holder = (ViewHolder) view.getTag();
            }
            holder.nombre.setText(contactosTemp.get(position).getNombre());
            if (contactosTemp.get(position).getFavorite() == 1)
            {
                holder.nombre.setTextColor(Color.BLUE);
                holder.tel.setTextColor(Color.BLUE);
            }
            else
            {
                holder.nombre.setTextColor(Color.BLACK);
                holder.tel.setTextColor(Color.BLACK);
            }
            holder.tel.setText(contactosTemp.get(position).getTelefono1());
            if(totalC%2==0)
            {
                holder.relativeLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.color1));
                totalC = totalC - 1;
            }
            else
            {
                holder.relativeLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.color2));
                totalC = totalC - 1;
            }
            holder.btnBorra.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
                    dialog.setTitle("Borrar contacto");
                    dialog.setMessage("¿Seguro que desea continuar?");
                    dialog.setCancelable(false);
                    dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ((ListaActivity) mContext).borrar(contactosTemp.get(position).get_ID());
                        }
                    });
                    dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) { } });
                    dialog.show();
                }
            });
            holder.btnModi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    try {
                        Bundle data = new Bundle();
                        data.putSerializable("contacto", contactosTemp.get(position));
                        Intent intent = new Intent(mContext, MainActivity.class);
                        intent.putExtras(data);
                        ((ListaActivity) mContext).finish();
                        mContext.startActivity(intent);
                    }
                    catch (Exception ex) {
                        Toast.makeText(mContext, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        catch (Exception e)
        {
            Toast.makeText(mContext, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return view;
    }
    public void filter (String critter)
    {
        critter = critter.toLowerCase();
        contactosTemp = new ArrayList<>();
        if (critter.length() == 0) {
            contactosTemp.addAll(contactos);
        }
        else {
            for (Contactos con : contactos)
            {
                if (con.getNombre().toLowerCase().contains(critter))
                {
                   contactosTemp.add(con);
                   updateData(contactosTemp);
                }
            }
            for (int i = 0; i < contactosTemp.size(); i++)
            {
                Toast.makeText(mContext, "contacto " +
                        contactosTemp.get(i).getNombre(), Toast.LENGTH_SHORT)
                        .show();
            }
        }
        notifyDataSetChanged();
    }
    public void updateData (ArrayList<Contactos> contactos) {
        this.contactosTemp = contactos;
        this.contactosList.clear();
        this.contactosList.addAll(this.contactosTemp);
    }


}
